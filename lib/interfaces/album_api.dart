import 'dart:async';

import 'package:carsomeflutterapp/models/album.dart';

// Class for unit testing
abstract class AlbumApi {
  Future<List<Album>> getAlbums(bool orientation);
}
