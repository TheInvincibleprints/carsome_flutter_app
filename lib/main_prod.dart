import 'package:carsomeflutterapp/main.dart';
import 'package:carsomeflutterapp/services/locator/locator.dart';
import 'package:carsomeflutterapp/utils/app_constants.dart';

import 'main.dart';

void main() {
  AppConstants.setEnvironment(Environment.PROD);
  setupLocator();
  mainDelegate();
}
