import 'dart:convert';

import 'package:carsomeflutterapp/models/album.dart';
import 'package:carsomeflutterapp/utils/app_constants.dart';

import 'network_util.dart';

class AlbumService {
  APIRequest apiRequest = new APIRequest();

  static final portraitUrl = AppConstants.SERVER + "photos?albumId=1";
  static final landscapeUrl = AppConstants.SERVER + "photos?albumId=2";

  static final AlbumService _internal = AlbumService.internal();

  factory AlbumService() => _internal;

  AlbumService.internal();

  @override
  Future<List<Album>> getAlbums(bool orientation) async {
    try {
      apiRequest
          .get(orientation ? portraitUrl : landscapeUrl)
          .then((dynamic response) {
        if (response != null) {
          List<Album> list = parsePhotos(response.body);

          return list;
        } else {
          throw Exception("Error fetching data");
        }
      });
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  // Parse the JSON response and return list of Album Objects //
  static List<Album> parsePhotos(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Album>((json) => Album.fromJson(json)).toList();
  }

  Future<AlbumResponse> getAlbumList(bool isPortrait) async {
    return apiRequest
        .get(isPortrait ? portraitUrl : landscapeUrl)
        .then((dynamic response) {
      if (response != null) {
        return AlbumResponse.fromJson(response);
      } else {
        if (response["errorMessage"] != null) {
          return Future.error(response["errorMessage"]);
        } else {
          return Future.error("Error while fetching data");
        }
      }
    });
  }
}
