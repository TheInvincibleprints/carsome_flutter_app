import 'package:carsomeflutterapp/services/network/network_provider.dart';
import 'package:carsomeflutterapp/utils/app_constants.dart';
import 'package:carsomeflutterapp/viewmodels/albums_view_model.dart';
import 'package:carsomeflutterapp/views/base/base_view.dart';
import 'package:carsomeflutterapp/views/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<AlbumViewModel>(
      builder: (context, child, model) => Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
          backgroundColor: Colors.blue,
          elevation: 0,
          centerTitle: true,
          title: new Text(
            '${AppConstants.APP_TITLE}',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Provider<NetworkProvider>(
          builder: (context) => NetworkProvider(),
          child: Consumer<NetworkProvider>(
            builder: (context, value, _) => Center(
              child: HomePage(
                networkProvider: value,
                viewModel: model,
                context: context,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
